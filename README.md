# Environment logger Data Storage Web Service 0.1.0

Web service that receives requests from clients to store and retrieve measurement data from the Environmental logger's project device.

To get started:
```
npm install
npm start 
```
For API usage info and examples view doc folder.
