import { IsString, IsEmail, IsNotEmpty, MinLength, MaxLength, IsEnum, IsOptional, Length } from 'class-validator';

export enum ROLE { USER = 'USER', ADMIN = 'ADMIN', }


export class CreateUserDto {
    @IsEmail() readonly email: string;
    @MinLength(6) @MaxLength(32) readonly password: string;
    @MinLength(2) @MaxLength(32) @IsString() readonly firstname: string;
    @MinLength(2) @MaxLength(64) @IsString() readonly lastname: string;
}

export class UpdateUserDto {
    @IsOptional() @IsString() id: string;
    @IsOptional() @IsEmail() readonly email: string;
    @IsOptional() @MinLength(6) @MaxLength(32) readonly password: string;
    @IsOptional() @MinLength(2) @MaxLength(32) @IsString() readonly firstname: string;
    @IsOptional() @MinLength(2) @MaxLength(64) @IsString() readonly lastname: string;
    @IsOptional() @IsEnum(ROLE) readonly role: string;
}

export class GetSingleUserDto {
    // Validation Allows Email and ID
    @IsNotEmpty() readonly id: string;
}


export class DeleteUserDto {
    @IsNotEmpty() readonly id: string;
}





export class GetUserDevicesDto {
    @IsNotEmpty() readonly id: string;
}

export class CreateUserDeviceDto {
    @IsNotEmpty() id: string;
    @IsNotEmpty() did: string;
}

export class DeleteUserDeviceDto {
    @IsNotEmpty() id: string;
    @IsNotEmpty() did: string;
}







export class GetUserFeedsDto {
    @IsNotEmpty() readonly id: string;
}

export class CreateUserFeedDto {
    @IsNotEmpty() readonly id: string;
    @IsNotEmpty() readonly did: string;
}

export class DeleteUserFeedDto {
    @IsNotEmpty() readonly id: string;
    @IsNotEmpty() readonly did: string;
}