import { Controller, Post, Get, Put, Delete, Body, Param, Request, UsePipes, UseGuards } from "@nestjs/common";
import { UsersService } from "./users.service";
import { CreateUserDto, UpdateUserDto, GetSingleUserDto, DeleteUserDto,
         GetUserDevicesDto, CreateUserDeviceDto, DeleteUserDeviceDto,
        GetUserFeedsDto, CreateUserFeedDto, DeleteUserFeedDto} from "./user.dto"
import { ValidationPipe } from "../common/validation.pipe"
import { JwtAuthGuard } from "../auth/jwt-auth.guard"


@Controller('api/users')
export class UsersController {
    constructor(private readonly usersService: UsersService) { }

    // Create User 
    @Post()
    //@UseGuards(JwtAuthGuard)
    @UsePipes(new ValidationPipe())
    cont_createUser(
        @Request() req, 
        @Body() createUserDto: CreateUserDto) {
        return this.usersService.createUser(createUserDto);
    }

    @Put(':id')
    @UseGuards(JwtAuthGuard)
    @UsePipes(new ValidationPipe())
    cont_updateUser(
        @Request() req, 
        @Param('id') id: string, 
        @Body() updateUserDto: UpdateUserDto
    ): object {
        // Update DTO with the id used to make the search 
        updateUserDto.id = id;
        return this.usersService.updateUser(updateUserDto)
    }

    // List Users
    @Get()
    cont_getAllUsers(
        @Request() req) {
        return this.usersService.getAllUsers();
    }

    // List User by ID or Email
    @Get(':id')
    @UsePipes(new ValidationPipe())
    async cont_getSingleUser(
        @Request() req, 
        @Param() getSingleUserDto: GetSingleUserDto) {
        return this.usersService.getSingleUser(getSingleUserDto);
    }

    // Delete User
    @Delete(':id')
    @UseGuards(JwtAuthGuard)
    @UsePipes(new ValidationPipe())
    cont_deleteUser(
        @Request() req, 
        @Param() deleteUserDto:DeleteUserDto): object {
        return this.usersService.deleteUser(deleteUserDto);
    }

    /**
     * 
     * USER DEVICE BIND
     * 
     */
    
    @Get(':id/binds')
    @UsePipes(new ValidationPipe())
    async cont_getUserDevices(
        @Request() req, 
        @Param() getUserDevicesDto:GetUserDevicesDto){
        return this.usersService.getUserDevices(getUserDevicesDto);
    }
    
    @Post(':id/binds/:did')
    @UseGuards(JwtAuthGuard)
    @UsePipes(new ValidationPipe())
    cont_createUserDevice(
        @Request() req, 
        @Param() createUserDeviceDto:CreateUserDeviceDto){
        return this.usersService.createUserDevice(createUserDeviceDto);
    }

    @Delete(':id/binds/:did')
    @UseGuards(JwtAuthGuard)
    @UsePipes(new ValidationPipe())
    cont_deleteUserDevice(
        @Request() req, 
        @Param() deleteUserDeviceDto:DeleteUserDeviceDto){
        return this.usersService.deleteUserDevice(deleteUserDeviceDto)
    }

    /**
     * 
     * FEEDS
     * 
     */
    /*
    @Get(':id/feeds')
    @UsePipes(new ValidationPipe())
    async cont_getUserFeeds(@Param('id') getUserFeedsDto:GetUserFeedsDto){
        return this.usersService.getUserFeeds(getUserFeedsDto);
    }
    
    @Post(':id/feeds/:fid')
    @UsePipes(new ValidationPipe())
    cont_createUserFeed(@Param('id') id: string, @Param('fid') createUserFeedDto:CreateUserFeedDto){
        return this.usersService.createUserFeed(createUserFeedDto);
    }

    @Delete(':id/feeds/:fid')
    @UsePipes(new ValidationPipe())
    cont_deleteUserFeed(@Param('id') id: string, @Param('fid') deleteUserFeedDto:DeleteUserDeviceDto){
        return this.usersService.deleteUserFeed(deleteUserFeedDto);
    }
    */
}