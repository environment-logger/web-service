import { Injectable, NotFoundException, InternalServerErrorException } from "@nestjs/common";
import { InjectModel } from '@nestjs/mongoose';
import { Model, MongoError } from 'mongoose'
import { User } from "./user.model";
import { Device } from "../devices/device.model";
import {
    CreateUserDto, GetSingleUserDto, UpdateUserDto, DeleteUserDto,
    GetUserDevicesDto, CreateUserDeviceDto, DeleteUserDeviceDto,
    GetUserFeedsDto, CreateUserFeedDto, DeleteUserFeedDto
} from "./user.dto";
import { Validator } from "class-validator";
import { bcryptCostants } from "../auth/constants"

@Injectable()
export class UsersService {
    constructor(
        @InjectModel('User') private readonly userModel: Model<User>,
        @InjectModel('Device') private readonly deviceModel: Model<Device>,
    ) { }

    /**
     * @description Inserts a user into the database
     * @returns Message containing the created user's id
     * @param createUserDto 
     */

    async createUser(createUserDto: CreateUserDto): Promise<Object> {
        const bcrypt = require('bcryptjs');
        let newUser = this.userModel(createUserDto);
        newUser.password = bcrypt.hashSync(createUserDto.password, bcryptCostants.salt); // Store hash of the password
        await newUser.save().then(function (savedData: User) {
        }).catch(function (err: MongoError) {
            if (err.code === 11000) {
                throw new InternalServerErrorException('Failed to create user, duplicate key error')
            } else {
                throw new InternalServerErrorException('Failed to create user')
            }
        });
        return { status: 'ok', message: 'User successfully created', data: newUser.id };
    }

    /**
     * @id
     * @param updateUserDto 
     */
    async updateUser(updateUserDto: UpdateUserDto): Promise<object> {
        const user = await this.findUser(updateUserDto.id).then(async function (usr) {
            if (updateUserDto.email) usr.email = updateUserDto.email;
            if (updateUserDto.password){
                const bcrypt = require('bcryptjs');
                usr.password = bcrypt.hashSync(updateUserDto.password, bcryptCostants.salt);  // Store hash of the password
            }
            if (updateUserDto.firstname) usr.firstname = updateUserDto.firstname;
            if (updateUserDto.lastname) usr.lastname = updateUserDto.lastname;
            if (updateUserDto.role) usr.role = updateUserDto.role;
            usr.modified = new Date().toISOString();

            await usr.save().catch(
                function (err: MongoError) {
                    throw new InternalServerErrorException('Failed to update the user');
                }
            );
        });
        return { status: 'ok', message: 'User successfully updated', data: user };
    }

    /**
     * @description 
     * @returns Message containing the list of users
     */
    async getAllUsers(): Promise<Object> {
        const users: User[] = await this.userModel.find().exec().catch(function (err: MongoError) {
            throw new InternalServerErrorException('Failed to retrieve users list');
        })
        return {
            status: 'ok', message: 'Users succesfully retrieved', data:
                users.map(usr => ({
                    id: usr.id,
                    email: usr.email,
                    password: usr.password,
                    firstname: usr.firstname,
                    lastname: usr.lastname,
                    devices: usr.devices,
                    feeds: usr.feeds,
                    role: usr.role,
                    token: usr.token,
                    modified: usr.modified,
                    created: usr.created
                }))
        };
    }

    /**
     * @description Retrieves user info based on his id or email
     * @param getSingleUserDto 
     * @returns Message containing the user data
     */
    async getSingleUser(getSingleUserDto: GetSingleUserDto): Promise<Object> {
        const id = getSingleUserDto.id.valueOf();
        let output;
        await this.findUser(id).then(function (res: User) {
            output = { status: 'ok', message: 'User succesfully retrieved', data: res };
        });
        return output;
    }

    /**
     * @description
     * @param id 
     * 
     */
    async deleteUser(deleteUserDto: DeleteUserDto): Promise<Object> {
        let user: Model<User>;

        user = await this.userModel.findById(deleteUserDto.id).exec().catch(function (err: MongoError) {
            throw new NotFoundException('Could not find user with id ' + deleteUserDto.id);
        });

        if (!user) throw new NotFoundException('Could not find user with id ' + deleteUserDto.id);

        this.deleteBoundDevices(user.devices);

        await this.userModel.deleteOne({ _id: deleteUserDto.id }).exec().catch(function (err: MongoError) {
            throw new InternalServerErrorException('Failed to delete user');
        }).then(function (res) {
            if (res.n === 0) throw new NotFoundException('Failed to delete, could not find user');
        })

        return { status: 'ok', message: 'User successfully deleted' };
    }


    private async deleteBoundDevices(devices: Array<string>): Promise<void> {
        devices.forEach(async element => {
            const device = await this.deviceModel.findById(element).exec().catch(function (err: MongoError) {
                throw new NotFoundException('Could not find bound device with id2 ' + element);
            });

            if (!device) throw new NotFoundException('Could not find bound device with id4 ' + element);

            await this.deviceModel.deleteOne({ _id: element }).exec().catch(function (err: MongoError) {
                throw new InternalServerErrorException('Failed to delete bound device');
            }).then(function (res) {
                if (!res) throw new NotFoundException('Failed to delete bound device, could not find device');
            })
        });
    }


    /**
     * @description Retrieves a user with a certain Id
     * @param id could be a MongoID or an email 
     * @returns Retrieved user
     */
    /*private*/ async findUser(id: string): Promise<Model<User>> {
        let user: User;

        // Check wether the id is a MongoDB identifier or an email
        const validator = new Validator();
        if (validator.isEmail(id)) {
            user = await this.userModel.findOne({ email: id }).exec().catch(function (err: MongoError) {
                throw new NotFoundException('Could not find user with id3 ' + id);
            });
        } else {
            user = await this.userModel.findById(id).exec().catch(function (err: MongoError) {
                throw new NotFoundException('Could not find user with id2 ' + id);
            });
        }
        if (!user) throw new NotFoundException('Could not find user with id4 ' + id);
        return user;
    }

    // DEVICES
    async getUserDevices(getUserDeviceDto: GetUserDevicesDto): Promise<Object> {
        let output: Object;
        await this.findUser(getUserDeviceDto.id).then(function (res: User) {
            output = { status: 'ok', message: 'User succesfully retrieved', data: res.devices };
        });
        return output;
    }

    public async createUserDevice(createUserDeviceDto: CreateUserDeviceDto): Promise<Object> {
        const user = await this.findUser(createUserDeviceDto.id).then(async function (usr) {
            if (usr.devices.indexOf(createUserDeviceDto.did) === -1) {
                usr.devices.push(createUserDeviceDto.did);
                usr.modified = new Date().toISOString();
                await usr.save().catch(
                    function (err: MongoError) {
                        throw new InternalServerErrorException('Failed to add the device');
                    }
                );
            } else {
                throw new InternalServerErrorException('Failed to add the device, duplicate key error')
            }
        });
        return { status: 'ok', message: 'Device successfully added', data: user };
    }

    public async deleteUserDevice(deleteUserDeviceDto: DeleteUserDeviceDto): Promise<Object> {
        const user = await this.findUser(deleteUserDeviceDto.id).then(async function (usr) {
            const index = usr.devices.indexOf(deleteUserDeviceDto.did);
            if (index != -1) {
                usr.devices.splice(index, 1);
                usr.modified = new Date().toISOString();
                await usr.save().catch(
                    function (err: MongoError) {
                        throw new InternalServerErrorException('Failed to remove the device');
                    }
                );
            } else {
                throw new InternalServerErrorException('Failed to remove the device, doesn´t exist')
            }
        });
        return { status: 'ok', message: 'Device successfully removed', data: user };
    }
    /*
    //FEEDS 
    async getUserFeeds(getUserFeedDto:GetUserFeedsDto):Promise<Object> {
        let output:Object;
        await this.findUser(getUserFeedDto.id).then(function(res:User){
            output =  {status: 'ok', message: 'User succesfully retrieved', data: res.feeds};
        });
        return output;
    }

    async createUserFeed(createUserFeedDto:CreateUserFeedDto):Promise<Object> {
        const user = await this.findUser(createUserFeedDto.id).then(async function(usr){
            if(usr.feeds.indexOf(createUserFeedDto.did) === -1){ 
                usr.feeds.push(createUserFeedDto.did);
                usr.modified = new Date().toISOString();
                await usr.save().catch(
                    function(err:MongoError){
                        throw new InternalServerErrorException('Failed to add the feed');
                    }
                );
            }else{
                throw new InternalServerErrorException('Failed to add the feed, duplicate key error')
            }
        });
        return { status: 'ok', message: 'Feed successfully added', data: user };
    }

    async deleteUserFeed(deleteUserFeedDto:DeleteUserFeedDto):Promise<Object> {
        const user = await this.findUser(deleteUserFeedDto.id).then(async function(usr){
            const index = usr.feeds.indexOf(deleteUserFeedDto.did);
            if( index != -1){ 
                usr.feeds.splice(index, 1);
                usr.modified = new Date().toISOString();
                await usr.save().catch(
                    function(err:MongoError){
                        throw new InternalServerErrorException('Failed to remove the feed');
                    }
                );
            }else{
                throw new InternalServerErrorException('Failed to remove the feed, doesn´t exist')
            }
        });
        return { status: 'ok', message: 'Feed successfully removed', data: user };
    }
    */
}