import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";

import { UsersController } from "./users.controller";
import { UsersService } from "./users.service";
import { UserSchema } from "./user.model";
import { DeviceSchema } from "../devices/device.model";



@Module({
    imports:[
        MongooseModule.forFeature([{name:'User',schema: UserSchema}]),
        MongooseModule.forFeature([{name:'Device',schema: DeviceSchema}]),
    ],
    controllers: [UsersController],
    providers: [UsersService],
    exports: [UsersService],
})
export class UsersModule { }