import * as mongoose from 'mongoose';
import {ROLE} from './user.dto';
 

// Database schema for the user
export const UserSchema = new mongoose.Schema({
    email: { type: String, required: true, lowercase:true, trim: true },
    password: { type: String, required: true },
    firstname:{ type: String, required: true, trim: true  },
    lastname: { type: String, required: true, trim: true  },
    devices: { type: Array, default: [String], required: false },
    feeds: { type: Array, default: [String], required: false },
    role: { type: String, enum :[ROLE.USER,ROLE.ADMIN], default: 'USER', required: false },
    token: { type: String, required: false },
    created: { type: Date, default: Date.now, required: false },
    modified: { type: Date, default: Date.now, required: false }

});

// Interface used for type-checking
export interface User extends mongoose.Document {
    readonly id: String;
    readonly email: String;
    readonly password: String;
    readonly firstname: String;
    readonly lastname: String;
    readonly devices: Array<Object>;
    readonly feeds: Array<Object>;
    readonly role: String;
    readonly token: String;
    readonly modified: Date;
    readonly created: Date;
}