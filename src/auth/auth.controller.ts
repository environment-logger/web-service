import {AuthService} from "./auth.service"
import { Controller, Request, Post,Body, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { LocalAuthGuard } from './local-auth.guard'

@Controller('api/auth/')
export class AuthController {
    constructor(private readonly authService: AuthService) { }

    // login
    //@UseGuards(LocalAuthGuard)
    @Post("login")
    async cont_login(@Request() req) {
        const usr = await this.authService.validateUser(req.body.email,req.body.password);
        const jwt = await this.authService.login(usr);
        return jwt;
    }

    // logout
    //@UseGuards(AuthGuard('local'))
    @Post("logout")
    cont_logout(@Request() req) {
        return req.body;
    }

    
}