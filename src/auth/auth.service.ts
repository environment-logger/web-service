import { Injectable, UnauthorizedException } from '@nestjs/common';
import { SessionsService } from '../sessions/sessions.service'
import { JwtService } from '@nestjs/jwt';
import { User } from 'src/users/user.model';
import { bcryptCostants } from "../auth/constants"
@Injectable()
export class AuthService {
  constructor(
    private readonly sessionsService: SessionsService,
    private readonly jwtService: JwtService,
  ) { }

  async validateUser(email: string, pass: string): Promise<User> {
    const bcrypt = require('bcryptjs');
    const user = await this.sessionsService.validate(email);
    if (user && user.password === bcrypt.hashSync(pass, bcryptCostants.salt)) {
      const { password, ...result } = user;
      return result;
    } else {
      throw new UnauthorizedException("Failed to authenticate.")
    }
    return null;
  }

  async login(user: any) {
    if (!user.email) throw new UnauthorizedException("Failed to authenticate.")
    const payload: object = { mail: user.email, uid: user._id, role: user.role };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}
