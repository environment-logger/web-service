import { IsString,IsNumber, IsObject,IsArray, IsBoolean, IsNotEmpty, IsOptional, MinLength, MaxLength, IsEnum, Length } from 'class-validator';
import {Measure} from './device.model';

export class CreateDeviceDto {
    @IsNotEmpty() readonly owner:string;
    @IsOptional() @IsNotEmpty() uid:string; // User ID **JWT GENERATED
    @IsOptional() @IsNotEmpty() role:string; // User role **JWT GENERATED
    @IsString() readonly name:string;
    @IsString() readonly city:string;
    @IsNumber() readonly longitude:Number;
    @IsNumber() readonly latitude:Number;
    //@IsArray() readonly light:Array<Object>;
    //@IsArray() readonly noise:Array<Object>;
    //@IsArray() readonly atmos:Array<Object>;
    @IsOptional() @IsBoolean() readonly active: Boolean;
}

export class UpdateDeviceDto {
    @IsOptional() @IsNotEmpty() id:string;
    @IsOptional() @IsNotEmpty() readonly owner:string; // only to check if user is authorized, not to be modified     
    @IsOptional() @IsNotEmpty() uid:string; // User ID **JWT GENERATED
    @IsOptional() @IsNotEmpty() role:string; // User role **JWT GENERATED
    @IsOptional() @IsString() readonly name:string;
    @IsOptional() @IsString() readonly city:string;
    @IsOptional() @IsNumber() readonly longitude:Number;
    @IsOptional() @IsNumber() readonly latitude:Number;
    @IsOptional() @IsArray() readonly light:Array<Measure>;
    @IsOptional() @IsArray() readonly noise:Array<Measure>;
    //@IsOptional() @IsArray() readonly atmos:Array<Measure>;
    @IsOptional() @IsArray() readonly temperature:Array<Measure>;
    @IsOptional() @IsArray() readonly humidity:Array<Measure>;
    @IsOptional() @IsArray() readonly co2:Array<Measure>;
    @IsOptional() @IsArray() readonly voc:Array<Measure>;
    @IsOptional() @IsBoolean() readonly active: Boolean;
}


export class GetSingleDeviceDto {
    @IsNotEmpty() readonly id:string;
    @IsOptional() @IsNotEmpty() role:string; // User role **JWT GENERATED
}

export class DeleteDeviceDto {
    @IsNotEmpty() readonly id:string;
    @IsOptional() @IsNotEmpty() uid:string; // User ID **JWT GENERATED
    @IsOptional() @IsNotEmpty() role:string; // User role **JWT GENERATED
}


export class AddMeasurementDto {
    @IsOptional() @IsNotEmpty() id:string; // Device ID 
    @IsOptional() @IsNotEmpty() uid:string; // User ID **JWT GENERATED
    @IsOptional() @IsNotEmpty() role:string; // User role **JWT GENERATED
    @IsOptional() @IsObject()  readonly light:Measure;
    @IsOptional() @IsObject()  readonly noise:Measure;
    @IsOptional() @IsObject()  readonly temperature:Measure;
    @IsOptional() @IsObject()  readonly humidity:Measure;
    @IsOptional() @IsObject()  readonly co2:Measure;
    @IsOptional() @IsObject()  readonly voc:Measure;    
    //@IsOptional() @IsObject()  readonly atmos:Measure;
}