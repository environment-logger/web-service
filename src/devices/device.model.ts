import * as mongoose from 'mongoose';
/*
const pointSchema = new mongoose.Schema({
    type: { type: String, enum: ['Point'], required: true },
    coordinates: { type: [Number], required: true }
});

const citySchema = new mongoose.Schema({
    name: String,
    //location: { type: pointSchema, required: true }
    location: { type: mongoose.Schema.Types.ObjectId, required: true }
});
const measureSchema = new mongoose.Schema({
    time: { type: Date, default: Date.now, required: false },
    servertime: { type: Date, default: Date.now, required: false },
    value: { type: [Number], required: true }
})
*/

export interface Measure {
    time: Date,
    servertime: string,
    value: Array<Number>
}

// Database schema for the device
export const DeviceSchema = new mongoose.Schema({
    owner: { type: String, required: true }, 
    name: { type: String, default:"My Logging Device", required: false },
    city: { type: String, required: true },
    longitude: { type: Number, required: true },
    latitude: { type: Number, required: true },
    light: { type: Array, default:[], required: false },
    noise: { type: Array, default:[], required: false },
    temperature: { type: Array, default:[], required: false },
    humidity: { type: Array, default:[], required: false },
    co2: { type: Array, default:[], required: false },
    voc: { type: Array, default:[], required: false },
    //atmos: { type: Array, default:[], required: false },
    active: { type: Boolean, default: false, required: false },
    modified: { type: Date, default: Date.now, required: false },
    created: { type: Date, default: Date.now, required: false }
});

// Interface used for type-checking
export interface Device extends mongoose.Document {
    readonly id: String;
    readonly owner: String;
    readonly name: String;
    readonly city: String;
    readonly longitude:Number;
    readonly latitude:Number;
    readonly light: Array<Measure>;
    readonly noise: Array<Measure>;
    //readonly atmos: Array<Measure>;
    readonly temperature: Array<Measure>;
    readonly humidity: Array<Measure>;
    readonly co2: Array<Measure>;
    readonly voc: Array<Measure>;
    readonly modified: Date;
    readonly created: Date;
    readonly active: Boolean;
}
