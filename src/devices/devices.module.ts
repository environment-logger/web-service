import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";

import { DevicesController } from "./devices.controller";
import { DevicesService } from "./devices.service";
import { DeviceSchema } from "./device.model";
import { UserSchema } from "../users/user.model";
import { UsersService } from "src/users/users.service";

@Module({
    imports:[
        MongooseModule.forFeature([{name:'Device',schema: DeviceSchema}]),
        MongooseModule.forFeature([{name:'User',schema: UserSchema}]),
        
    ],
    controllers: [DevicesController],
    providers: [DevicesService],
    exports: [DevicesService],
})
export class DevicesModule { }