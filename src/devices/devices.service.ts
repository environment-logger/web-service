import { Injectable, NotFoundException, InternalServerErrorException, BadRequestException, UnauthorizedException } from "@nestjs/common";
import { InjectModel } from '@nestjs/mongoose';
import { Model, MongoError } from 'mongoose'
import { Device } from "./device.model";
import { UsersService } from "../users/users.service";
import { User } from "../users/user.model";

import { CreateUserDeviceDto, DeleteUserDeviceDto } from "../users/user.dto"
import { CreateDeviceDto, GetSingleDeviceDto, UpdateDeviceDto, AddMeasurementDto, DeleteDeviceDto } from "./device.dto";


@Injectable()
export class DevicesService {
    constructor(
        @InjectModel('Device') private readonly deviceModel: Model<Device>,
        @InjectModel('User') private readonly userModel: Model<User>
    ) { }

    async createDevice(createDeviceDto: CreateDeviceDto): Promise<Object> {
        const newDevice = this.deviceModel(createDeviceDto);
        this.isUserOwner(createDeviceDto.owner, createDeviceDto.uid);
        await newDevice.save().then(function (savedData: Device) {
        }).catch(function (err: MongoError) {
            throw new InternalServerErrorException('Failed to create device')
        });

        this.bind(newDevice, createDeviceDto);

        return { status: 'ok', message: 'Device successfully created', data: newDevice.id };
    }

    private async bind(newDevice, createDeviceDto: CreateDeviceDto): Promise<void> {
        // Device created, bind to user
        let createUserDeviceDto: CreateUserDeviceDto = new CreateUserDeviceDto();
        createUserDeviceDto.id = createDeviceDto.owner;
        createUserDeviceDto.did = newDevice.id;

        // Add Device to User.devices
        let usersService: UsersService = new UsersService(this.userModel, this.deviceModel);
        await usersService.createUserDevice(createUserDeviceDto).catch(function (err) {
            // TODO If errors rollback
            throw new InternalServerErrorException('Failed to bind device to user')
        });
    }

    async updateDevice(updateDeviceDto: UpdateDeviceDto): Promise<object> {
        const dvc: Model<Device> = await this.findDevice(updateDeviceDto.id);
        this.isUserOwner(dvc.owner, updateDeviceDto.uid);
        if (updateDeviceDto.name) dvc.name = updateDeviceDto.name;
        if (updateDeviceDto.city) dvc.city = updateDeviceDto.city;
        if (updateDeviceDto.longitude) dvc.longitude = updateDeviceDto.longitude;
        if (updateDeviceDto.latitude) dvc.latitude = updateDeviceDto.latitude;
        if (updateDeviceDto.active) dvc.active = updateDeviceDto.active;
        if (updateDeviceDto.light) dvc.light = updateDeviceDto.light;
        if (updateDeviceDto.noise) dvc.noise = updateDeviceDto.noise;
        //if (updateDeviceDto.atmos) dvc.atmos = updateDeviceDto.atmos;
        if (updateDeviceDto.temperature) dvc.temperature = updateDeviceDto.temperature;
        if (updateDeviceDto.humidity) dvc.humidity = updateDeviceDto.humidity;
        if (updateDeviceDto.co2) dvc.co2 = updateDeviceDto.co2;
        if (updateDeviceDto.voc) dvc.voc = updateDeviceDto.voc;
        dvc.modified = new Date().toISOString();

        await dvc.save().catch(
            function (err: MongoError) {
                throw new InternalServerErrorException('Failed to update the device');
            }
        );

        return { status: 'ok', message: 'Device successfully updated', data: dvc };;
    }

    async getAllDevices(role: string): Promise<Object> {
        // TODO : USE role TO FILTER OUTPUT
        const devices: Device[] = await this.deviceModel.find().exec().catch(function (err: MongoError) {
            throw new InternalServerErrorException('Failed to retrieve devices list');
        })

        let output: object;

        output = {
            status: 'ok', message: 'Devices succesfully retrieved', data:
                devices.map(dvc => ({
                    id: dvc.id,
                    name: dvc.name,
                    owner: dvc.owner,
                    city: dvc.city,
                    latitude: dvc.latitude,
                    longitude: dvc.longitude,
                    active: dvc.active,
                    light: dvc.light,
                    noise: dvc.noise,
                    //atmos: dvc.atmos,
                    temperature: dvc.temperature,
                    humidity: dvc.humidity,
                    co2: dvc.co2,
                    voc: dvc.voc,
                    modified: dvc.modified,
                    created: dvc.created
                }))
        };

        return output;
    }


    async getSingleDevice(getSingleDeviceDto: GetSingleDeviceDto): Promise<Object> {
        // TODO : USE getSingleDeviceDto.role TO FILTER OUTPUT
        const id = getSingleDeviceDto.id.valueOf();
        let output;
        await this.findDevice(id).then(function (res: Device) {
            output = { status: 'ok', message: 'Device succesfully retrieved', data: res };
        });
        return output;
    }


    async deleteDevice(deleteDeviceDto: DeleteDeviceDto): Promise<Object> {
        let device: Model<Device>;
        device = await this.deviceModel.findById(deleteDeviceDto.id).exec().catch(function (err: MongoError) {
            throw new NotFoundException('Could not find device with id ' + deleteDeviceDto.id);
        });

        this.isUserOwner(device.owner, deleteDeviceDto.uid);

        if (!device) throw new NotFoundException('Could not find device with id ' + deleteDeviceDto.id);

        this.unbind(device, deleteDeviceDto);

        await this.deviceModel.deleteOne({ _id: deleteDeviceDto.id }).exec().catch(function (err: MongoError) {
            throw new InternalServerErrorException('Failed to delete device');
        }).then(function (res) {
            if (!res) throw new NotFoundException('Failed to delete, could not find device');
        })

        return { status: 'ok', message: 'Device successfully deleted' };
    }

    private async unbind(deletedDevice, deleteDeviceDto: DeleteDeviceDto) {
        let deleteUserDeviceDto: DeleteUserDeviceDto = new DeleteUserDeviceDto();
        deleteUserDeviceDto.id = deletedDevice.owner;
        deleteUserDeviceDto.did = deleteDeviceDto.id;

        // remove Device to User.devices
        let usersService: UsersService = new UsersService(this.userModel, this.deviceModel);
        await usersService.deleteUserDevice(deleteUserDeviceDto).catch(function (err) {
            // TODO If errors rollback
            throw new InternalServerErrorException('Failed to unbind device from user')
        });
    }


    async addMeasurement(addMeasurementDto: AddMeasurementDto): Promise<Object> {
        const dvc: Model<Device> = await this.findDevice(addMeasurementDto.id);
        this.isUserOwner(dvc.owner, addMeasurementDto.uid);

        if (addMeasurementDto.light || addMeasurementDto.noise || addMeasurementDto.temperature|| addMeasurementDto.humidity|| addMeasurementDto.co2|| addMeasurementDto.co2){//|| addMeasurementDto.atmos) {
            const now = new Date().toISOString();
            if (addMeasurementDto.light) {
                addMeasurementDto.light.servertime = now;
                dvc.light.push(addMeasurementDto.light);
            }
            if (addMeasurementDto.noise) {
                addMeasurementDto.noise.servertime = now;
                dvc.noise.push(addMeasurementDto.noise);
            }
            /*
            if (addMeasurementDto.atmos) {
                addMeasurementDto.atmos.servertime = now;
                dvc.atmos.push(addMeasurementDto.atmos);
            }*/
            
            if (addMeasurementDto.temperature) {
                addMeasurementDto.temperature.servertime = now;
                dvc.temperature.push(addMeasurementDto.temperature);
            }
            if (addMeasurementDto.humidity) {
                addMeasurementDto.humidity.servertime = now;
                dvc.humidity.push(addMeasurementDto.humidity);
            }
            if (addMeasurementDto.co2) {
                addMeasurementDto.co2.servertime = now;
                dvc.co2.push(addMeasurementDto.co2);
            }
            if (addMeasurementDto.voc) {
                addMeasurementDto.voc.servertime = now;
                dvc.voc.push(addMeasurementDto.voc);
            }


            dvc.modified = now;
            await dvc.save().catch(
                function (err: MongoError) {
                    throw new InternalServerErrorException('Failed to add the measurement');
                }
            );
        } else {
            throw new BadRequestException('Failed to add Measurement, no data');
        }
        return { status: 'ok', message: 'Measurement successfully added' };
    }

    private async findDevice(id: string): Promise<Model<Device>> {
        let device: Device;
        device = await this.deviceModel.findById(id).exec().catch(function (err: MongoError) {
            throw new NotFoundException('Could not find device with id2 ' + id);
        });
        if (!device) throw new NotFoundException('Could not find device with id4 ' + id);
        return device;
    }

    private isUserOwner(usr, ownr) {
        //console.log(usr + " " + ownr);
        if (usr != ownr) throw new UnauthorizedException("User must be the owner of the device to perform this operation");
    }
}