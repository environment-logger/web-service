import { Controller, Post, Get, Put, Delete, Body, Param, Header,Request, UsePipes, UseGuards, BadRequestException  } from "@nestjs/common";
import { DevicesService } from "./devices.service";
import { CreateDeviceDto, UpdateDeviceDto, GetSingleDeviceDto, AddMeasurementDto, DeleteDeviceDto } from "./device.dto"
import { ValidationPipe } from "../common/validation.pipe";
import { JwtAuthGuard } from "../auth/jwt-auth.guard";
import {jwtConstants} from "../auth/constants"
import { sign, verify } from 'jsonwebtoken';

@Controller('api/devices')
export class DevicesController {
    constructor(private readonly devicesService: DevicesService) { }
    
    // Create Device 
    @Post()
    @UseGuards(JwtAuthGuard)
    @UsePipes(new ValidationPipe())
    cont_createDevice(
        @Request() req, 
        @Body() createDeviceDto: CreateDeviceDto) {
            // Get jwt token to Recover logged user id
            const jwtToken = this.retrieveToken(req.headers.authorization);
            createDeviceDto.uid = verify(jwtToken,jwtConstants.secret,{})['uid']; // user email
        return this.devicesService.createDevice(createDeviceDto);
    }
    
    @Put(':id')
    @UseGuards(JwtAuthGuard)
    @UsePipes(new ValidationPipe())
    cont_updateDevice(
        @Request() req, 
        @Param('id') id: string, 
        @Body() updateDeviceDto: UpdateDeviceDto
    ): object {
        // Update DTO with the id used to make the search 
        updateDeviceDto.id = id;
        // Get jwt token to Recover logged user id
        const jwtToken = this.retrieveToken(req.headers.authorization);//req.headers.authorization.split(' ').pop();
        updateDeviceDto.uid = verify(jwtToken,jwtConstants.secret,{})['uid']; // user email
        updateDeviceDto.role = verify(jwtToken,jwtConstants.secret,{})['role'];
        return this.devicesService.updateDevice(updateDeviceDto)
    }
    
    // List Devices
    @Get()
    cont_getAllDevices(@Request() req) {
        const jwtToken = this.retrieveToken(req.headers.authorization);
        return this.devicesService.getAllDevices(verify(jwtToken,jwtConstants.secret,{})['role']); // pass role User and Admin can see different things
    }
    
    // List Device by ID
    @Get(':id')
    @UsePipes(new ValidationPipe())
    async cont_getSingleDevice(
        @Request() req,
        @Param() getSingleDeviceDto: GetSingleDeviceDto) {

       const jwtToken = this.retrieveToken(req.headers.authorization);
       getSingleDeviceDto.role = verify(jwtToken,jwtConstants.secret,{})['role'];
       return this.devicesService.getSingleDevice(getSingleDeviceDto);
    }
    
    // Delete Device
    @Delete(':id')
    @UseGuards(JwtAuthGuard)
    @UsePipes(new ValidationPipe())
    cont_deleteDevice(
        @Request() req, 
        @Param() deleteDeviceDto: DeleteDeviceDto): object {
        // Get jwt token to Recover logged user id
        const jwtToken = this.retrieveToken(req.headers.authorization);
        deleteDeviceDto.uid = verify(jwtToken,jwtConstants.secret,{})['uid'];
        deleteDeviceDto.role = verify(jwtToken,jwtConstants.secret,{})['role'];
        return this.devicesService.deleteDevice(deleteDeviceDto);
    }


    // Add a measurement
    @Post(':id/measurements')
    @UseGuards(JwtAuthGuard)
    @UsePipes(new ValidationPipe())
    cont_addMeasurement(
        @Request() req, 
        @Param('id') id:string, 
        @Body() addMeasurementDto: AddMeasurementDto) {
        addMeasurementDto.id = id;
        const jwtToken = this.retrieveToken(req.headers.authorization);
        addMeasurementDto.uid = verify(jwtToken,jwtConstants.secret,{})['uid'];
        addMeasurementDto.role = verify(jwtToken,jwtConstants.secret,{})['role'];
        return this.devicesService.addMeasurement(addMeasurementDto);
    }
    
    /**
     * Retrieves authorization bearer string
     * @param auth 
     * @returns 
     */
    private retrieveToken(auth:string):string{
        if(!auth){throw new BadRequestException("Invalid token");}
        const bearer = auth.split(' ').pop();
        if(!bearer){throw new BadRequestException("Invalid token");}
        return bearer;        
    }
}