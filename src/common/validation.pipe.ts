import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from '@nestjs/common';
import { validate, ValidationError } from 'class-validator';
import { plainToClass } from 'class-transformer';

@Injectable()
export class ValidationPipe implements PipeTransform<any> {
  async transform(value, metadata: ArgumentMetadata) {
    const { metatype } = metadata;
    if (!metatype || !this.toValidate(metatype)) {
      return value;
    }
    const object = plainToClass(metatype, value);
    const errors = await validate(object);
    // Format validation error message
    // https://www.npmjs.com/package/class-validator#validation-errors
    if (errors.length > 0) {
      let errorMsg = 'Validation failed';
      if (errors[0].constraints["isString"]) {
        errorMsg += ", " + errors[0].constraints["isString"];
      } else if (errors[0].constraints["isNotEmpty"]) {
        errorMsg += ", " + errors[0].constraints["isNotEmpty"];
      } else if (errors[0].constraints["isEmail"]) {
        errorMsg += ", " + errors[0].constraints["isEmail"];
      } else if (errors[0].constraints["minLength"]) {
        errorMsg += ", " + errors[0].constraints["minLength"];
      } else if (errors[0].constraints["maxLength"]) {
        errorMsg += ", " + errors[0].constraints["maxLength"];
      } else if (errors[0].constraints["length"]) {
        errorMsg += ", " + errors[0].constraints["length"];
      } else if (errors[0].constraints["isEnum"]) {
        errorMsg += ", " + errors[0].constraints["isEnum"];
      } else if (errors[0].constraints["isBoolean"]) {
        errorMsg += ", " + errors[0].constraints["isBoolean"];
      } else if (errors[0].constraints["isObject"]) {
        errorMsg += ", " + errors[0].constraints["isObject"];
      } else if (errors[0].constraints["isNumber"]) {
        errorMsg += ", " + errors[0].constraints["isNumber"];
      }
      
      throw new BadRequestException(errorMsg);

    }
    return value;
  }


  private toValidate(metatype): boolean {
    const types = [String, Boolean, Number, Array, Object];
    return !types.find(type => metatype === type);
  }
}