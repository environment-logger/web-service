import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from "@nestjs/mongoose";
import { UsersModule } from './users/users.module';
import { DevicesModule} from './devices/devices.module';
import { AuthModule } from './auth/auth.module';
import { SessionsModule } from './sessions/sessions.module';
import { DeviceSchema } from "./devices/device.model";
import { UserSchema } from "./users/user.model";

@Module({
  imports: [
    UsersModule, 
    DevicesModule,
    AuthModule,
    SessionsModule,
    MongooseModule.forRoot(
      'mongodb+srv://testuser:testpass@cluster0-eyd7c.mongodb.net/dlogger-demo?retryWrites=true&w=majority',
      { useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true }
    ),
    MongooseModule.forFeature([{name:'Device',schema: DeviceSchema}]),
    MongooseModule.forFeature([{name:'User',schema: UserSchema}]),
  ],
  controllers: [AppController],
  providers: [
    AppService,
  ],
})
export class AppModule {}
