import { Injectable, NotFoundException, InternalServerErrorException  } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, MongoError } from 'mongoose'
import { User } from "../users/user.model";
import { Validator } from "class-validator";

export type Session = any;

@Injectable()


export class SessionsService {
    constructor(
        @InjectModel('User') private readonly userModel: Model<User>
    ) { }

    async validate(id:string): Promise<Session | undefined> {
        let user = await this.userModel.findOne({ email: id }).exec().catch(function(err:MongoError){
            throw new NotFoundException('Could not find user with id ' + id);
        });
        if (!user) throw new NotFoundException('Could not find user with id ' + id);
        
        return user._doc;
    }

}
