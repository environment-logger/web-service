import { Module } from '@nestjs/common';
import { SessionsService } from "./sessions.service";
import { MongooseModule } from "@nestjs/mongoose";
import { UserSchema } from "../users/user.model";

@Module({
  imports:[
    MongooseModule.forFeature([{name:'User',schema: UserSchema}]),
  ],
  providers: [SessionsService],
  exports: [SessionsService],
})
export class SessionsModule {}
